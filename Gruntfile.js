/*global module */

module.exports = function (grunt) {
  "use strict";

  grunt.initConfig({

    concat: {
      options: {
        separator: ';'
      },
      application: {
        src: [
          'src/js/**/*.js'
        ],
        dest: 'dist/application.js'
      },
      libraries: {
        src: [
          'node_modules/jquery/dist/jquery.js',
          'node_modules/mustache/mustache.js',
          'libraries/**/*.js'
        ],
        dest: 'dist/libraries.js'
      },
      stylesheets: {
        src: [
          'node_modules/bootstrap/dist/css/bootstrap.css',
          'libraries/**/*.css'
        ],
        dest: 'dist/style.css'
      }
    },

    jsdoc: {
      dist: {
        src: [
          'src/**/*.js'
        ],
        dest: 'dist/doc/'
      }
    },

    copy: {
      statics: {
        expand: true,
        flatten: true,
        filter: 'isFile',
        src: [
          'src/*'
        ],
        dest: 'dist/'
      }
    },

    watch: {
      options: {
        event: [
          'added',
          'changed',
          'deleted'
        ]
      },
      scripts: {
        files: [
          'src/**/*.js'
        ],
        tasks: [
          'concat',
          'jsdoc'
        ],
        options: {
          spawn: false
        }
      },
      libraries: {
        files: 'libraries/**/*.js',
        tasks: 'concat',
        options: {
          spawn: false
        }
      },
      stylesheets: {
        files: [
          'node_modules/bootstrap/dist/css/bootstrap.css',
          'libraries/**/*.css'
        ],
        tasks: [
          'concat'
        ]
      },
      statics: {
        files: [
          'src/*'
        ],
        tasks: [
          'copy'
        ],
        options: {
          spawn: false
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-jsdoc');
  grunt.loadNpmTasks('grunt-clear');

  grunt.registerTask('default', [
    'clear',
    'concat',
    'copy',
    'watch'
    // 'jsdoc'
  ]);

};