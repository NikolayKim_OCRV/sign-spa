/*global $, console, Model, View, Controller */

function Application() {
  "use strict";

  console.debug("Создан экземпляр приложения");

  var
    m_view = new View(),
    m_model = new Model(),
    m_controller = new Controller(m_model, m_view);

  /**
   * Start application
   */
  this.start = function () {
    m_controller.initialize();
  };

}

$(document).ready(function () {
  "use strict";

  console.info("Начало работы...");

  var
    m_application = new Application();

  m_application.start();

});