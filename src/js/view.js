/*global $, console, Mustache */
/*jslint regexp:true */

/**
 * Представление
 * @class
 */
function View() {
  "use strict";

  console.debug("Создан экземпляр представления");

  this.previousPage = undefined;

  $('.app-pg')
    .hide()
    .removeClass('hidden');

}

/**
 * Осуществить переход на страницу
 * @param {String} page Идентификатор страницы
 */
View.prototype.navigateTo = function (page) {
  "use strict";

  $('.app-pg').hide();
  $('#' + page).show();

};

View.prototype.bind = function (event, handler) {
  "use strict";

  function onClick(selector, callback) {
    $(document).on('click', selector, callback);
  }

  switch (event) {

  case 'SelectCertificate':
    onClick('.pgCertItem', function () {
      handler($(this).attr('id'));
    });
    break;

  case 'Cancel':
    onClick("#pgCertCancel", handler);
    break;

  case 'GoSign':
    onClick('#pgCertContinue', handler);
    break;

  case 'Sign':
    onClick('#pgSignSign', handler);
    break;

  case 'ReturnToCertList':
    onClick('.returnToCertList', handler);
    break;

  case 'GoSave':
    onClick('#pgSignSave', handler);
    break;

  default:
    console.error("Событие {0} не определено в ракурсе, невозможно подписать контроллер на него".format(event));

  }

};

/**
 * Отрисовать элемент интерфейса
 * @param {String} part       Идентификатор элемента
 * @param {Object} parameters Параметры операции (индивидуально)
 */
View.prototype.render = function (part, parameters) {
  "use strict";

  var
    self = this,
    parts;

  function renderTemplate(name) {
    $("#{0}".format(name))
      .empty()
      .html(
        Mustache.to_html(
          $("#{0}Template".format(name)).html(),
          parameters
        )
      );
  }

  parts = {

    initMessage: function (messageClass) {
      $("#pgInitMessageTitle").text(parameters.title || "");
      $("#pgInitMessageText").text(parameters.text || "");
      $("#pgInitMessage")
        .removeClass()
        .addClass("alert")
        .addClass(messageClass);
    },

    initInfo: function () {
      this.initMessage("alert-info");
    },

    initSuccess: function () {
      this.initMessage("alert-success");
    },

    initError: function () {
      this.initMessage("alert-danger");
    },

    certList: function () {
      renderTemplate('pgCertList');
      $("#pgCertContinue").addClass("disabled");
    },

    certActive: function () {
      $('.pgCertItem').removeClass('active');
      $('#' + parameters.thumbprint).addClass('active');
      $("#pgCertContinue").removeClass("disabled");
    },

    signCertInfo: function () {
      renderTemplate("pgSignCertInfo");
    },

    signDocList: function () {
      renderTemplate('pgSignDocList');
    },

    signRenderControls: function () {
      renderTemplate('pgSignControls');
    },

    pgSave: function () {
      renderTemplate('pgSaveReport');
    }

  };

  if (typeof parts[part] === "function") {
    parts[part]();
  } else {
    console.error("Метод отрисовки {0} не определён в ракурсе".format(part));
  }

};