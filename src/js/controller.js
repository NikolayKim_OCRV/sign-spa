/*global $, console, delay, CryptoPro */

function Controller(model, view) {
  "use strict";

  console.debug("Создан экземпляр контроллера");

  this.model = model;
  this.view = view;

  this.memoryId = undefined;
  this.certificate = undefined;

  this.previousPage = undefined;

  this.serviceURL = (function () {
    return "{0}//{1}/{2}".format(
      window.location.protocol,
      window.location.host,
      window.location.hostname.match(/(\.test)$/) !== null ? "api.php" : "sap/bc/z00wdpdsign"
    );
  }());

}

/**
 * Запланировать переход между страницами на основе состояния приложения
 */
Controller.prototype.navigate = function (cancel) {
  "use strict";

  var
    self = this,
    page;

  if (cancel === true) {
    page = 'pgCancel';
  } else {

    if (CryptoPro.IsInitialized && self.model.certStore !== undefined && self.model.docPackage !== undefined && self.model.certStore.getCertificateCount() > 0 && self.model.docPackage.documents.length > 0) {
      if (self.certificate === undefined) {
        page = 'pgCert';
      } else {
        if (self.model.docPackage.isSigned() === false) {
          page = 'pgSign';
        } else {
          page = 'pgSave';
        }
      }
    } else {
      page = 'pgInit';
    }

  }

  if (this.previousPage !== page) {
    this.previousPage = page;

    console.info("Переход на страницу {0}".format(page));

    this.view.navigateTo(page);
    self.updateView();

  }

};


Controller.prototype.bindEvents = function () {
  "use strict";

  var
    self = this;

  console.log("Привязка обработчиков контроллера к событиям представления");

  self.view.bind('SelectCertificate', function (thumbprint) {
    self.selectCertificate(thumbprint);
  });

  self.view.bind('GoSign', function () {
    if (self.certificate !== undefined) {
      self.navigate();
    }
  });

  self.view.bind('Sign', function () {
    self.signPackage();
    self.updateDocumentList();
    self.updateSignControls();
  });

  self.view.bind('ReturnToCertList', function () {
    self.returnToCertificateList();
  });

  self.view.bind('GoSave', function () {
    if (self.model.docPackage.isSigned()) {
      self.transferSignatures();
    }
  });

  self.view.bind('Cancel', function () {
    self.navigate(true); // Cancel
  });

};

Controller.prototype.initialize = function () {
  "use strict";

  var
    self = this,
    searchQuery;

  self.bindEvents();
  self.navigate();

  try {

    // Идентификатор SAP Memory ID

    self.view.render("initInfo", {
      title: "Чтение настроек..."
    });

    searchQuery = window.location.search.match(/memoryID=([\w%]+)/i);
    if (searchQuery === null) {
      throw new Error("Неправильный параметр");
    } else {
      this.memoryId = decodeURIComponent(searchQuery[1]);
      console.info("Параметр memoryID = {0}".format(self.memoryId));
    }

    // КриптоПро

    self.view.render("initInfo", {
      title: "Загрузка КриптоПро..."
    });

    console.debug('Инициализация КриптоПро');
    CryptoPro.Initialize();

    // Сертификаты

    self.view.render("initInfo", {
      title: "Чтение списка сертификатов..."
    });

    console.debug('Чтение хранилища сертификатов');
    self.model.loadCertificates(CryptoPro.CertificateStore());

    // Документы

    self.view.render("initInfo", {
      title: "Загрузка документов..."
    });

    $.ajax({
      url: "{0}?memoryID={1}".format(self.serviceURL, self.memoryId),
      method: "GET",
      async: false,
      success: function (data) {
        self.model.loadPackage(typeof data === "object" ? data : $.parseJSON(data));
      },
      error: function (xhr, status, error) {
        console.log("GET {0}: {1}".format(status.capitalizeFirstLetter(), error));
      }
    });

    if (self.model.docPackage === undefined || self.model.docPackage.documents.length === 0) {
      throw new Error("Не удалось получить документы из SAP");
    }

    self.view.render("initSuccess", {
      title: "Загрузка успешно завершена!",
      text: "Пожалуйста подождите..."
    });

    // Переход на следующую страницу

    self.navigate();

  } catch (e) {

    self.view.render("initError", {
      title: "Ошибка:",
      text: e.message
    });

    console.error(e.message);

  }

};

Controller.prototype.updateCertificateList = function () {
  "use strict";

  var
    self = this,
    templateData = {
      certificates: []
    };

  if (self.model.certStore === undefined) {
    return;
  }

  console.log("Перечисление списка сертификатов");

  $.each(self.model.certStore.getCertificates(), function (k, c) {
    templateData.certificates.push({
      thumbprint: c.getThumbprint(),
      title: c.getTitle(),
      post: c.getPost(),
      isQualified: c.isQualified(),
      hasPrivateKey: c.hasPrivateKey(),
      isExpired: c.isExpired(),
      isFuture: c.isFuture(),
      validFrom: c.getValidFromDate(),
      validTo: c.getValidToDate(),
      isValid: c.isValid(),
      isInnValid: c.isInnValid()
    });
  });

  self.view.render('certList', templateData);

};

Controller.prototype.selectCertificate = function (thumbprint) {
  "use strict";

  var
    self = this,
    cert;

  if ((self.certificate = cert = self.model.certStore.getCertificate(thumbprint)) !== undefined) {

    console.info("Выбран сертификат {0}".format(cert.toString()));

    self.view.render('certActive', {
      thumbprint: cert.getThumbprint()
    });

    self.view.render('signCertInfo', {
      title: cert.getTitle(),
      post: cert.getPost(),
      validFrom: cert.getValidFromDate(),
      validTo: cert.getValidToDate()
    });

  } else {
    console.error("Не удалось получить объект сертификата {0}".format(thumbprint));
  }

};

Controller.prototype.updateDocumentList = function () {
  "use strict";

  var
    self = this,
    docList = [];

  if (self.model.docPackage === undefined) {
    return;
  }

  self.view.render('signDocList', {
    documents: (function () {
      $.each(self.model.docPackage.documents, function (k, d) {
        docList.push({
          docName: d.getDocName(),
          isSigned: d.isSigned()
        });
      });
      return docList;
    }())
  });

};

Controller.prototype.updateSignControls = function () {
  "use strict";

  var
    self = this;

  if (self.model.docPackage === undefined) {
    return;
  }

  self.view.render('signRenderControls', {
    signComplete: self.model.docPackage.isSigned()
  });

};

Controller.prototype.signPackage = function () {
  "use strict";

  var
    self = this;

  console.info("Подписание документов");

  $.each(self.model.docPackage.documents, function (k, d) {
    if (d.isSigned() === false) {
      try {
        d.setSignature(
          CryptoPro.Sign({
            cadesType: 0x01,
            data: d.getData(),
            thumbprint: self.certificate.getThumbprint()
          })
        );
        console.log("Документ {0} успешно подписан".format(d.getDocName()));
      } catch (e) {
        console.warn("Документ {0} не был подписан: {1}".format(
          d.getDocName(),
          e.message
        ));
      }
    } else {
      console.log("Документ {0} уже подписан".format(d.getDocName()));
    }
  });

};

Controller.prototype.returnToCertificateList = function () {
  "use strict";

  var
    self = this;

  self.model.docPackage.resetSignatures();

  self.certificate = undefined;
  self.updateCertificateList();

  self.navigate();

};

Controller.prototype.updateView = function () {
  "use strict";

  var
    self = this;

  try {

    self.updateCertificateList();
    self.updateDocumentList();
    self.updateSignControls();

  } catch (e) {
    console.warn("Ошиба при обновлении представления: {0}".format(e.message));
  }

};

Controller.prototype.transferSignatures = function () {
  "use strict";

  var
    self = this;

  self.navigate();

  console.info("Передача подписей в SAP...");

  self.view.render('pgSave', {
    progress: true
  });

  $.ajax({
    url: "{0}?memoryID={1}".format(self.serviceURL, encodeURIComponent(self.memoryId)),
    method: "POST",
    async: false,
    contentType: 'application/json',
    data: JSON.stringify({
      documents: self.model.docPackage.pack()
    }),
    success: function (data) {
      console.info("Подписи успешно переданы в SAP");
      self.view.render('pgSave', {
        progress: false,
        success: true
      });
    },
    error: function (xhr, status, error) {
      console.log("POST {0}: {1}".format(status.capitalizeFirstLetter(), error));
      self.view.render('pgSave', {
        progress: false,
        success: false,
        message: error
      });
    }
  });

};