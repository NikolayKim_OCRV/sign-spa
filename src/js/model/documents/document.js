/*global $*/

/**
 * Представляет одиночный документ в приложении.
 * @class
 */

function Document(sapJSON) {
  "use strict";

  var
    m_docID = sapJSON.docid,
    m_signNum = sapJSON.signnum,
    m_role = sapJSON.role,
    m_content = sapJSON.content,
    m_docName = sapJSON.docname,

    m_signature;

  if (m_content.length === 0) {
    throw new Error("Данные документа {0} не переданы".format(m_docID));
  }

  /**
   * Генерирует описание документа в формате SAP.
   * @returns {Object} Описание документа SAP
   */
  this.pack = function () {
    return {

      // Пересылаемые поля SAP

      docid: m_docID,
      signnum: m_signNum,
      role: m_role,
      content: m_content,

      // Заполненные приложением поля

      eds: m_signature

    };
  };

  /**
   * Возвращает имя документа или номер
   * @returns {String} Имя документа или номер
   */
  this.getDocName = function () {
    return m_docName || m_docID;
  };

  /**
   * Возвращает данные документа.
   * @returns {String} Документ в кодировке Base-64
   */
  this.getData = function () {
    return m_content;
  };

  /**
   * Сохраняет данные подписи в документе.
   * @param {String} signature Подпись в кодировке Base-64
   */
  this.setSignature = function (signature) {
    m_signature = signature.replace(/(?:\\[rn]|[\r\n]+)+/g, "");
  };

  /**
   * Стирает созданную ранее подпись.
   */
  this.resetSignature = function () {
    m_signature = undefined;
  };

  /**
   * Проверяет, подписан ли документ.
   * @returns {Boolean} True если подписан
   */
  this.isSigned = function () {
    return m_signature === undefined ? false : true;
  };

}