/*global $, console, Document, CI */

function DocPackage(sapJSON) {
  "use strict";

  var
    self = this,
    m_jsonData = sapJSON;

  console.log("Обработка пакета документов");

  self.documents = [];

  $.each(m_jsonData.documents, function (k, v) {
    self.documents.push(new Document(v));
  });

  console.info("{0} {1} {2}".format(
    CI.Spell(
      self.documents.length,
      CI.Words.received
    ),
    self.documents.length,
    CI.Spell(
      self.documents.length,
      CI.Words.documents
    )
  ).capitalizeFirstLetter());

  $.each(self.documents, function (k, v) {
    console.log("Документ №{0}: {1}".format(
      k + 1,
      v.getDocName()
    ));
  });

}

/**
 * Проверяет, подписаны ли все документы в пакете.
 * @returns {Boolean} [[Description]]
 */
DocPackage.prototype.isSigned = function () {
  "use strict";

  var
    self = this,
    signed = true;

  $.each(self.documents, function (k, d) {
    if (d.isSigned() === false) {
      return (signed = false);
    }
  });

  return signed;

};

/**
 * Стирает ранее созданные подписи
 */
DocPackage.prototype.resetSignatures = function () {
  "use strict";

  var
    self = this;

  $.each(self.documents, function (k, d) {
    d.resetSignature();
  });

};

/**
 * Экспортирует данные документов в формат SAP
 * @returns {Array} Описания документов
 */
DocPackage.prototype.pack = function () {
  "use strict";

  var
    self = this,
    data = [];

  $.each(self.documents, function (k, d) {
    data.push(d.pack());
  });

  return data;

};