/*global console, DocPackage, CertStore */

function Model() {
  "use strict";

  console.debug("Создан экземпляр модели");

  this.docPackage = undefined;
  this.certStore = undefined;

}

/**
 * Разобрать пакет документов SAP для подписания
 * @param {String} sapJSON JSON
 */
Model.prototype.loadPackage = function (sapJSON) {
  "use strict";
  this.docPackage = new DocPackage(sapJSON);
};

/**
 * Перечислить и разобрать сертификаты
 * @param {Object} certStore Хранилище сертификатов Windows
 */
Model.prototype.loadCertificates = function (certStore) {
  "use strict";
  this.certStore = new CertStore(certStore);
};