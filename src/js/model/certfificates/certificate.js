/*global moment, console */

function Certificate(certiicate) {
  "use strict";

  /**
   * Производит разбор поля Субъект и возвращает значение указанного поля.
   * @param   {String} subject Поле Субъект сертификата
   * @param   {String} field   Наименование поля
   * @returns {String} Значение поля
   */
  function getSubjectField(subject, field) {
    var
      matches = [];
    if ((matches = subject.match("{0}=([^,]+)".format(field))) !== null) {
      return matches[1];
    } else {
      return undefined;
    }
  }

  /**
   * Возвращает значение СНИЛС из поля Субъект.
   * @param   {String} subject Поле Субъект сертификата
   * @returns {String} Значение поля СНИЛС
   */
  function getSnils(subject) {
    return getSubjectField(subject, "OID.1.2.643.100.3") ||
      getSubjectField(subject, "SNILS") ||
      getSubjectField(subject, "СНИЛС");
  }

  /**
   * Возвращает значение ОГРН из поля Субъект.
   * @param   {String} subject Поле Субъект сертификата
   * @returns {Strubg} Значение поля ОГРН
   */
  function getOgrn(subject) {
    return getSubjectField(subject, "1.2.643.100.1") ||
      getSubjectField(subject, "OGRN") ||
      getSubjectField(subject, "ОГРН");
  }

  /**
   * Возвращает значение ИНН из поля Субъект.
   * @param   {String} subject Поле Субъект сертификата
   * @returns {Strubg} Значение поля ИНН
   */
  function getInn(subject) {
    return getSubjectField(subject, "1.2.643.3.131.1.1") ||
      getSubjectField(subject, "INN") ||
      getSubjectField(subject, "ИНН");
  }

  /**
   * Возвращает ФИО подписанта с учётом полей сертификата ЮЛ
   * @param   {String} subject Поле Субъект сертификата
   * @returns {String} ФИО подписанта
   */
  function getSigner(subject) {
    if (!!getOgrn(subject)) {
      return "{0} {1}".format(
        getSubjectField(subject, 'SN'),
        getSubjectField(subject, 'G')
      ) || getSubjectField(subject, 'CN');
    } else {
      return getSubjectField(subject, 'CN');
    }
  }

  /**
   * Производит проверку СНИЛС по контрольным цифрам.
   * @param   {String}  snils СНИЛС
   * @returns {Boolean} True, если номер верный
   */
  function validateSnils(snils) {

    if (snils === undefined || snils.length === 0) {
      return false;
    }

    var
      checkSum = parseInt(snils.slice(9), 10),
      sum;

    snils = snils.toString().split('');
    sum = (snils[0] * 9 + snils[1] * 8 + snils[2] * 7 + snils[3] * 6 + snils[4] * 5 + snils[5] * 4 + snils[6] * 3 + snils[7] * 2 + parseInt(snils[8], 10));

    if (sum < 100 && sum === checkSum) {
      return true;
    } else if ((sum === 100 || sum === 101) && checkSum === 0) {
      return true;
    } else if (sum > 101 && (sum % 101 === checkSum || (sum % 101 === 100 && checkSum === 0))) {
      return true;
    } else {
      return false;
    }

  }

  /**
   * Производит проверку ИНН по контрольным цифрам.
   * @param   {String}  inn  ИНН
   * @param   {String}  ogrn ОГРН
   * @returns {Boolean} True, если номер верный
   */
  function validateInn(inn, ogrn) {

    inn = parseInt(inn, 10).toString().split('');

    if (inn.length == 11) {
      inn.unshift('0');
    }

    // Для ИНН в 10 знаков

    if (ogrn !== undefined && (inn.length === 10) && (parseInt(inn[9], 10) === ((2 * inn[0] + 4 * inn[1] + 10 * inn[2] + 3 * inn[3] + 5 * inn[4] + 9 * inn[5] + 4 * inn[6] + 6 * inn[7] + 8 * inn[8]) % 11) % 10)) {
      return true;

      // Для ИНН в 12 знаков

    } else if (ogrn === undefined && (inn.length === 12) && ((parseInt(inn[10], 10) === ((7 * inn[0] + 2 * inn[1] + 4 * inn[2] + 10 * inn[3] + 3 * inn[4] + 5 * inn[5] + 9 * inn[6] + 4 * inn[7] + 6 * inn[8] + 8 * inn[9]) % 11) % 10) && (parseInt(inn[11], 10) === ((3 * inn[0] + 7 * inn[1] + 2 * inn[2] + 4 * inn[3] + 10 * inn[4] + 3 * inn[5] + 5 * inn[6] + 9 * inn[7] + 4 * inn[8] + 6 * inn[9] + 8 * inn[10]) % 11) % 10))) {
      return true;
    } else {
      return false;
    }

  }

  // Разбор сертификата

  var
    m_cert = certiicate,
    m_subject = m_cert.SubjectName,

    m_descr = {
      thumbprint: m_cert.Thumbprint,
      hasPrivateKey: m_cert.HasPrivateKey(),

      snils: getSnils(m_subject),
      ogrn: getOgrn(m_subject),
      inn: getInn(m_subject),

      title: getSigner(m_subject),
      post: getSubjectField(m_subject, ', T'),

      validFrom: new Date(m_cert.ValidFromDate),
      validTo: new Date(m_cert.ValidToDate)

    };

  console.log("Сертификат {0}: {1} {2}, СНИЛС {3}, ОГРН {4}, ИНН {5}, Ключ {6}, Действителен с {7} по {8}".format(
    m_descr.thumbprint,
    m_descr.title,
    m_descr.post,
    m_descr.snils,
    m_descr.ogrn,
    m_descr.inn,
    m_descr.hasPrivateKey,
    m_descr.validFrom,
    m_descr.validTo
  ));

  function formatDate(d) {
    return "{0}.{1}.{2}".format(
      d.getDate() < 9 ? "0" + d.getDate() : d.getDate(),
      d.getMonth() + 1 < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1,
      d.getFullYear()
    );
  }

  /**
   * Возвращает отпечаток сертификата.
   * @returns {String} Отпечаток
   */
  this.getThumbprint = function () {
    return m_descr.thumbprint;
  };

  /**
   * Возвращает ФИО подписанта.
   * @returns {String} ФИО
   */
  this.getTitle = function () {
    return m_descr.title;
  };

  /**
   * Возвращает должность подписанта.
   * @returns {String} Должность
   */
  this.getPost = function () {
    return m_descr.post;
  };

  /**
   * Возвращает СНИЛС подписанта.
   * @returns {String} СНИЛС
   */
  this.getSnils = function () {
    return m_descr.snils;
  };

  /**
   * Возвращает отформатированную дату начала действия сертификата.
   * @returns {String} Сертификата действителен с...
   */
  this.getValidFromDate = function () {
    return formatDate(m_descr.validFrom);
  };

  /**
   * Возвращает отформатированную дату конца действия сертификата.
   * @returns {String} Сертификата действителен по...
   */
  this.getValidToDate = function () {
    return formatDate(m_descr.validTo);
  };

  /**
   * Проверяет, является ли сертификат квалифицированным.
   * @returns {Boolean} Квалифицированный
   */
  this.isQualified = function () {
    return validateSnils(m_descr.snils);
  };

  /**
   * Проверяет, является ли ИНН верным
   * Для сертификатов, выданных до 01.07.2015 наличие поля ИНН не обязательно
   * @returns {Boolean} ИНН правильный
   */
  this.isInnValid = function () {
    return (m_descr.inn === undefined && m_descr.validFrom < new Date('Wed Jul 01 2015 03:00:00 GMT+0300')) || validateInn(m_descr.inn, m_descr.ogrn);
  };

  /**
   * Проверяет, просрочен ли сертификат.
   * @returns {Boolean} Просрочен
   */
  this.isExpired = function () {
    return (new Date() > m_descr.validTo);
  };

  /**
   * Проверяет, не находится ли дата начала действия сертификата в будущем.
   * @returns {Boolean} Срок действия ещё не наступил
   */
  this.isFuture = function () {
    return (new Date() < m_descr.validFrom);
  };

  /**
   * Проверяет, действителен ли сертификат на текущую дату.
   * @returns {Boolean} Действителен на текущую дату
   */
  this.isActive = function () {
    return !(this.isExpired() || this.isFuture());
  };

  /**
   * Проверяет, связан ли сертификат с закрытым ключом
   * @returns {Boolean} Сертификат связан с закрытым ключом
   */
  this.hasPrivateKey = function () {
    return m_descr.hasPrivateKey;
  };

  /**
   * Проверяет, является ли сертификат действительным для подписания.
   * @returns {Boolean} Действителен для подписания
   */
  this.isValid = function () {
    return this.hasPrivateKey() && this.isActive() && this.isQualified() && this.isInnValid();
  };

  /**
   * Возвращает текстовое описание сертификата
   * @returns {String} Описание
   */
  this.toString = function () {
    return "{0} {1} ({2}) {3}-{4}".format(
      this.getThumbprint(),
      this.getTitle(),
      this.getPost(),
      this.getValidFromDate(),
      this.getValidToDate()
    );
  };

}