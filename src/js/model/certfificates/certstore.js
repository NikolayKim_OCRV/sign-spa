/*global $, console, Certificate, CI */

/**
 * Представляет список с описаниями сертификатов.
 * @class
 * @param {Object} certificateStore Объект храналища сертификатов
 */
function CertStore(certificateStore) {
  "use strict";

  var
    m_store = certificateStore,
    m_certs = [],
    i;

  try {

    console.log('Доступ к хранилищу сертификатов');
    m_store.Open();

    if (m_store.Certificates.Count === 0) {
      throw new Error('В системе не установлено ни одного сертификата');
    } else {
      console.info('{0} {1} {2}'.format(
        CI.Spell(
          m_store.Certificates.Count,
          CI.Words.found
        ),
        m_store.Certificates.Count,
        CI.Spell(
          m_store.Certificates.Count,
          CI.Words.certificate
        )
      ).capitalizeFirstLetter());
    }

    // Выбирать сертификаты только с заполненными полями ФИО и Должность

    (function () {
      var
        cert;
      for (i = m_store.Certificates.Count; i > 0; i -= 1) {
        cert = new Certificate(m_store.Certificates.Item(i));
        if (cert.getTitle() !== undefined && cert.getPost() !== undefined) {
          m_certs.push(cert);
        }
      }
    }());

    console.info('{0} {1} {2}'.format(
      CI.Spell(
        m_certs.length,
        CI.Words.selected
      ),
      m_certs.length,
      CI.Spell(
        m_certs.length,
        CI.Words.certificate
      )
    ).capitalizeFirstLetter());

  } catch (e) {
    throw (e);
  }

  /**
   * Возвращает список разобранных сертификатов.
   * @returns {Array} Сертификаты
   */
  this.getCertificates = function () {
    return m_certs;
  };

  /**
   * Возвращает количество сертификатов.
   * @returns {Number} [[Description]]
   */
  this.getCertificateCount = function () {
    return m_certs.length;
  };

  /**
   * Возвращает сертификат с указанным отпечатком
   * @param   {String}  thumbprint Отпечаток
   * @returns {Boolean} Объект сертификата
   */
  this.getCertificate = function (thumbprint) {
    var
      cert;

    $.each(m_certs, function (k, c) {
      if (c.getThumbprint() === thumbprint) {
        cert = c;
        return false;
      }
    });

    return cert;
  };

}