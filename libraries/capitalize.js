// http://stackoverflow.com/questions/1026069/capitalize-the-first-letter-of-string-in-javascript

/**
 * Capitalizes 1st letter in a string.
 * @memberof Utilities
 * @returns {String} 1st letter capitalized
 */
String.prototype.capitalizeFirstLetter = function () {
  "use strict";
  return this.charAt(0).toUpperCase() + this.slice(1);
};